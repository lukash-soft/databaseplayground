package com.example.DbPlayground;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbPlaygroundApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbPlaygroundApplication.class, args);
	}

}
