package com.example.DbPlayground.models.productTax;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "tr_products_tax")
public class ProductTax {
    @Id
    public Long taxId;
    public Long taxValue;
}
