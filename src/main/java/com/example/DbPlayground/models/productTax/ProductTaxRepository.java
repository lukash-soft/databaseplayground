package com.example.DbPlayground.models.productTax;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductTaxRepository extends JpaRepository<ProductTax, Long> {
}
