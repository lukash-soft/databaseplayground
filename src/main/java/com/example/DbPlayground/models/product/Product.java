package com.example.DbPlayground.models.product;

import com.example.DbPlayground.models.productTax.ProductTax;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "tr_products")
public class Product {
    @Id
    @Column(name = "PROD_ID")
    public Long prodId;
    public String prodName;
    @OneToOne
    @JoinColumn(name= "PROD_FAMILY_ID")
    public Product productFamily;
    public Long annualLicencePrice;
    @OneToOne
    @JoinColumn(name = "TAX_ID")
    public ProductTax taxId;
}
