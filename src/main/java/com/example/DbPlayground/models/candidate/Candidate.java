package com.example.DbPlayground.models.candidate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "tr_candidates")
public class Candidate {
    @Id
    public Long candidateId;
    public String firstName;
    public String lastName;
    public Long testResult;
}
