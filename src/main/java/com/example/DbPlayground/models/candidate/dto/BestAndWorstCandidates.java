package com.example.DbPlayground.models.candidate.dto;

import com.example.DbPlayground.models.candidate.Candidate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BestAndWorstCandidates {
    private List<Candidate> bestCandidates;
    private List<Candidate> worstCandidates;
}
