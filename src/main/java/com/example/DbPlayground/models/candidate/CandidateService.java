package com.example.DbPlayground.models.candidate;

import com.example.DbPlayground.models.candidate.dto.BestAndWorstCandidates;
import com.example.DbPlayground.models.testScore.TestScore;
import com.example.DbPlayground.models.testScore.TestScoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class CandidateService {

    private final CandidateRepository candidateRepository;
    private final TestScoreRepository testScoreRepository;

    public Map<String, Long> countCandidates() {
        List<TestScore> allTestScores = testScoreRepository.findAll();
        Map<String, Long> result = new HashMap<>();
        for (TestScore testScore : allTestScores) {
            long countCandidates = candidateRepository.countForResultsBetween(testScore.getPointsMin(), testScore.getPointsMax());
            result.put(testScore.getGrade(), countCandidates);
        }
        return result;
    }

    public List<Candidate> findAllCandidatesWhoPassed() {
        long minScoreToPass = testScoreRepository.findTestScoreToPass();
        return candidateRepository.findAllWhoseScoreAtLeast(minScoreToPass);
    }

    public List<Candidate> findAllWithParticularGrade(TestScore testScore) {
        long pointsMin = testScore.getPointsMin();
        long pointsMax = testScore.getPointsMax();
        return candidateRepository.findAllWithScoreBetween(pointsMin, pointsMax);
    }

    public List<Candidate> findAllSortedByFirstNameAndLastName() {
        return candidateRepository.findAllSortedByLastNameAndFirstName();
    }

    public BestAndWorstCandidates findBestAndWorstCandidates() {

        int minScore = testScoreRepository.findMinTestScore();
        int maxScore = testScoreRepository.findMaxTestScore();

        List<Candidate> best = candidateRepository.findBestCandidates(maxScore);
        List<Candidate> worst = candidateRepository.findWorstCandidates(minScore);

        return BestAndWorstCandidates.builder()
                .bestCandidates(best)
                .worstCandidates(worst)
                .build();
    }
}
