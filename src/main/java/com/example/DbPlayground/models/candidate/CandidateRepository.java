package com.example.DbPlayground.models.candidate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {

    @Query(value = "SELECT * FROM tr_candidates WHERE TEST_RESULT = :maxScore limit 3", nativeQuery = true)
    List<Candidate> findBestCandidates(int maxScore);
    @Query(value = "SELECT * FROM tr_candidates WHERE TEST_RESULT = :minScore limit 3", nativeQuery = true)
    List<Candidate> findWorstCandidates(int minScore);

    @Query(value = "SELECT COUNT(*) FROM tr_candidates WHERE TEST_RESULT >= :testScoreMin AND TEST_RESULT <= :testScoreMax", nativeQuery = true)
    Long countForResultsBetween(long testScoreMin, long testScoreMax);

    @Query(value = "SELECT c FROM Candidate c WHERE c.testResult >= :limitNumber")
    List<Candidate> findAllWhoseScoreAtLeast(long limitNumber);

    @Query(value = "SELECT * FROM tr_candidates WHERE TEST_RESULT >= :pointsMin and TEST_RESULT <= :pointsMax", nativeQuery = true)
    List<Candidate> findAllWithScoreBetween(long pointsMin, long pointsMax);

    @Query(value = "SELECT * FROM tr_candidates ORDER BY LAST_NAME, FIRST_NAME", nativeQuery = true)
    List<Candidate> findAllSortedByLastNameAndFirstName();


}
