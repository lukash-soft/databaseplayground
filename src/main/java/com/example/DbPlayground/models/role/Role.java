package com.example.DbPlayground.models.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "tr_roles")
public class Role {

    @Id
    public Long roleId;
    public String roleName;
    public Long maxSalary;
    public Long minSalary;
}
