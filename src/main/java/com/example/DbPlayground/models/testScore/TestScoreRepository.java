package com.example.DbPlayground.models.testScore;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TestScoreRepository extends JpaRepository<TestScore, Long> {

    @Query(value = "select * from test_scores where GRADE = :gradeName",nativeQuery = true)
    Optional<TestScore> findByGrade(String gradeName);

    @Query(value = "select max(POINTS_MAX) from test_scores", nativeQuery = true)
    Integer findMaxTestScore();

    @Query(value = "select min(POINTS_MIN) from test_scores", nativeQuery = true)
    Integer findMinTestScore();

    @Query(value = "select min(POINTS_MIN) from test_scores where GRADE = 'Partially pass'", nativeQuery = true)
    Integer findTestScoreToPass();
}
