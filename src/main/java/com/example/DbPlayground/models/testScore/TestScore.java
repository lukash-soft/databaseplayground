package com.example.DbPlayground.models.testScore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "test_scores")
public class TestScore {
    @Id
    public Long gradeId;
    public String grade;
    public Long pointsMax;
    public Long pointsMin;

}
