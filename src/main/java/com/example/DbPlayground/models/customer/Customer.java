package com.example.DbPlayground.models.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "tr_customers")
public class Customer {
    @Id
    @Column(name = "CUST_ID")
    public Long custId;
    public String custName;

}
