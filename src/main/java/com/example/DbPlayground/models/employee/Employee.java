package com.example.DbPlayground.models.employee;

import com.example.DbPlayground.models.role.Role;
import com.example.DbPlayground.models.team.Team;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "tr_employees")
public class Employee {
    @Id
    @Column(name = "EMPLOYEE_ID")
    public Long employeeId;
    public String firstName;
    public String lastName;
    public String phone;
    public String email;
    public LocalDate hireDate;
    public Long salary;
    @OneToOne
    public Employee manager;
    @OneToOne
    public Role role;
    @ManyToOne
    public Team team;

}
