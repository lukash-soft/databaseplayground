package com.example.DbPlayground.models.productOrder;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductOrderRepository extends JpaRepository<ProductOrder, Long> {

    @Query(value = "SELECT * FROM product_orders WHERE ORDER_STATUS = :paidStatus", nativeQuery = true)
    List<ProductOrder> findAllByPaidStatus(@Param("paidStatus")String paidStatus);
}
