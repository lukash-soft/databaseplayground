package com.example.DbPlayground.models.productOrder;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductOrderService {

    private final ProductOrderRepository productOrderRepository;

    public List<ProductOrder> findAllUnpaidOrders() {
        //Comment to this task
        //I didn't know how to create Enum so that the repo would return me the List<ProductOrders>.
        //It always returned empty List. I used normal enum classes and enum of Strings. Didn't work either way.
        // That's why I decided to hardcode the orderStatus

        String orderStatus = "Ordered";
        return productOrderRepository.findAllByPaidStatus(orderStatus);
    }
}
