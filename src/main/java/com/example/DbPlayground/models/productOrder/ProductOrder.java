package com.example.DbPlayground.models.productOrder;

import com.example.DbPlayground.models.customer.Customer;
import com.example.DbPlayground.models.product.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "product_orders")
public class ProductOrder {
    @Id
    public Long orderId;
    @OneToOne
    @JoinColumn(name = "PROD_ID")
    public Product product;
    public int quantity;
    public String orderStatus;
    @OneToOne
    @JoinColumn(name = "CUST_ID")
    public Customer customer;
    public LocalDate orderDate;
}
