package com.example.DbPlayground.models.productOrder;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class ProductOrderServiceTest {

    @Autowired
    ProductOrderService productOrderService;

    @Test
    void findAllUnpaidOrders() {
        List<ProductOrder> productOrderListUnpaid = productOrderService.findAllUnpaidOrders();
        assertThat(productOrderListUnpaid).isNotEmpty();
    }
}
