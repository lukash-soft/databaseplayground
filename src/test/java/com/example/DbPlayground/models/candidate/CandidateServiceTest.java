package com.example.DbPlayground.models.candidate;

import com.example.DbPlayground.models.candidate.dto.BestAndWorstCandidates;
import com.example.DbPlayground.models.testScore.TestScore;
import com.example.DbPlayground.models.testScore.TestScoreRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class CandidateServiceTest {

    @Autowired
    CandidateService candidateService;
    @Autowired
    TestScoreRepository testScoreRepository;


    @Test
    void countCandidates() {
        Map<String, Long> result = candidateService.countCandidates();
        assertThat(result).isNotEmpty();
    }

    @Test
    void findlAllWhoPassed() {
        List<Candidate> result = candidateService.findAllCandidatesWhoPassed();
        assertThat(result).isNotEmpty();
    }

    @Test
    void findAllWhoHaveTestResults() {
        TestScore testScoreExcellent = testScoreRepository.findByGrade("Excellent").orElseThrow();
        List<Candidate> result = candidateService.findAllWithParticularGrade(testScoreExcellent);
        assertThat(result).isNotEmpty();
    }

    @Test
    void findAllSortedByFirstNameAndLastName() {
        List<Candidate> result = candidateService.findAllSortedByFirstNameAndLastName();
        assertThat(result).isNotEmpty();
    }

    @Test
    void findBestAndWorstCandidates() {
        BestAndWorstCandidates bestAndWorstCandidates = candidateService.findBestAndWorstCandidates();
        assertThat(bestAndWorstCandidates).isNotNull();
    }
}
